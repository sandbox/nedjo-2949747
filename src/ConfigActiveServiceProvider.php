<?php

namespace Drupal\config_active;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

use Symfony\Component\DependencyInjection\Reference;

/**
 * Modifies the Config Filter storage factory service.
 */
class ConfigActiveServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('config_filter.storage_factory');
    // Replace the sync storage with the active storage.
    $definition->replaceArgument(0, new Reference('config.storage'));
  }
}
